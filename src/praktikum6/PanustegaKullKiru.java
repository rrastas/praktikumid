package praktikum6;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import lib.TextIO;
import praktikum5.Meetod2;

public class PanustegaKullKiru {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int kasutajaRaha = 100;

		while (kasutajaRaha > 0) {
			System.out.println("Palun sisesta panus (Max 25)"); 
			int maksimaalnePanus = Math.min(25, kasutajaRaha);
			int panus = Meetod2.kasutajaSisestus(1, 25);
			kasutajaRaha -= panus;

			int Kullv6iKiri = AraArvamisMang.suvalineArv(0, 1);

			if (Kullv6iKiri == 0) { // Kull
				System.out.println("VÕITSID, saad topelt raha tagasi");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("kaotasid...");
			}
			System.out.println("Sul on " + kasutajaRaha + " raha");
		}

	}

}
