package praktikum3;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double keskmineHinne;
		int l6put66Hinne;
		
		// Ctrl + tühik
		
		// && -- loogiline JA
		// || -- loogiline VÕI
		
		System.out.println("Mis on su keskmine hinne?");
		keskmineHinne = TextIO.getlnDouble();
		if (keskmineHinne > 5 || keskmineHinne < 0) {
			System.out.println("Pole võimalik!");
			return;
		}
		System.out.println("Mis on su lõputöö hinne?");
		l6put66Hinne = TextIO.getlnInt();
		if (l6put66Hinne > 5 || l6put66Hinne < 0) {
			System.out.println("Pole võimalik!");
			return;
		}
		
		if (keskmineHinne > 4.5 && l6put66Hinne == 5) {
			System.out.println("Jah saad cum laude diplomile!");
		} else {
			System.out.println("Ei saa cum laudet.");
		}

	}

}
