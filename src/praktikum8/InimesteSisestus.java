package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimesteSisestus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		String nimi = new String("Mati");
//		Inimene Kati = new Inimene("Kati", 24);
//		//keegi.tervita();
//		Inimene Juri = new Inimene("Juri", 23);
//		Inimene Peeter = new Inimene("Peeter", 25);
//		
//		
//		Kati.tervita();
//		Juri.tervita();
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		while(true){
		System.out.println("Sisesta nimi ja vanus");
		String nimi = TextIO.getlnString();
		if(nimi.equals(""))
			break;
		int vanus = TextIO.getlnInt();
		Inimene keegi = new Inimene(nimi,vanus);
		inimesed.add(keegi);
		}
		
		for(Inimene inimene : inimesed){
			//Java kutsub välja Inimene klassi toString() meetodi
			System.out.println(inimene);
		}
//
//		for (Inimene inimene : inimesed) {
//		    // Java kutsub välja Inimene klassi toString() meetodi
//		    System.out.println(inimene);
//		}
		
		
		
	}

}
