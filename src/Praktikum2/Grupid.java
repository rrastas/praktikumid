package Praktikum2;

import lib.TextIO;

public class Grupid {
	public static void main(String[] args) {
		//sysout -> ctrl + tühik
		System.out.println("Palun sisesta inimeste arv ");
		int inimesteArv = TextIO.getlnInt();
		
		System.out.println("Palun sisesta grupi suurus ");
		int grupiSuurus = TextIO.getlnInt();
		
		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Moodustada saab  " + gruppideArv + " gruppi. ");
		
		// 5 jagada 2-ga = 2 ja jääk 1.
		// %
		int j22k = inimesteArv % grupiSuurus;
		System.out.println("Üle jääb " + j22k + " inimest. ");
	}

}
