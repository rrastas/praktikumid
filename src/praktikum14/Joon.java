package praktikum14;

public class Joon {

	Punkt algpunkt;
	Punkt l6pppunkt;

	public Joon(Punkt minuPunkt, Punkt veelYksPunkt) {
		this.algpunkt = minuPunkt;
		this.l6pppunkt = veelYksPunkt;
	}

	public double pikkus() {
		double a = l6pppunkt.y - algpunkt.y;
		double b = l6pppunkt.x - algpunkt.x;
		System.out.println("a on " + a + " ja b on " + b);
		double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		return c;

	}

	@Override
	public String toString() {
		return "Joon (" + algpunkt + "," + l6pppunkt + ")";

	}
}
