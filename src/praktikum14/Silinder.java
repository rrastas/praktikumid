package praktikum14;

public class Silinder extends Ring {

	double k6rgus;

	public Silinder(Ring alus, double h) {
		super(alus.keskpunkt, alus.raadius);
		k6rgus = h;
	}

	private double kyljePindala() {
		return k6rgus * ymberm66t();
	}

	@Override
	public double pindala() {
		return 2 * super.pindala() + kyljePindala();
	}
}
