package praktikum14;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Ring {

	Punkt keskpunkt;
	double raadius;

	public Ring(Punkt keskpunkt, double r) {
		this.keskpunkt = keskpunkt;
		raadius = r;
	}

	public double ymberm66t() {
		return 2 * Math.PI * raadius;
	}

	@Override
	public String toString() {
		NumberFormat formatter = new DecimalFormat("#0.00");
		return "Ring, ümbermõõduga " + formatter.format(ymberm66t()) + " ja pindalaga " + formatter.format(pindala());
	}

	public double pindala() {
		return Math.PI * raadius * raadius;
	}
}
