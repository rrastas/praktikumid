package praktikum14;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Katsetused {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 100;
		minuPunkt.y = 200;

		Punkt veelYksPunkt = new Punkt(200, 300);

		System.out.println(minuPunkt);
		System.out.println(veelYksPunkt);

		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		
		NumberFormat formatter = new DecimalFormat("#0.00");  
		System.out.println("Joone pikkus on: " + formatter.format(minuJoon.pikkus()));
		
		
		Ring minuRing = new Ring(minuPunkt, 50);
		System.out.println(minuRing);
		
		Silinder minuSilinder = new Silinder(minuRing, 100);
		System.out.println(minuSilinder);

	}

}
